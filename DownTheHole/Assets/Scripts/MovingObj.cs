﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObj : MonoBehaviour
{
    public float speed;
    public float arriveDistance;
    public List<Vector3> positions;
    public float waitTimer;
    public float innerWaitTimer;

    private Rigidbody2D rb;
    public int nextPos;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        foreach(Transform child in transform)
        {
            positions.Add(child.transform.position);
        }
        

    }

    // Update is called once per frame
    void Update()
    {
        Movement();
        WaitAtArrival();
    }

    void Movement()
    {
      if(innerWaitTimer<=0)
        {
            if (Vector3.Distance(transform.position, positions[nextPos]) > arriveDistance)
            {
                transform.position = Vector3.MoveTowards(transform.position, positions[nextPos], speed * Time.deltaTime);
            }
            else
            {
                innerWaitTimer = waitTimer;
                if (nextPos + 1 >= positions.Count)
                {
                    nextPos = 0;
                }
                else
                {
                    nextPos += 1;
                }

            }
        }
    

    }



    void WaitAtArrival()
    {
        if (innerWaitTimer > 0)
        {
            innerWaitTimer -= Time.deltaTime;
        }

    }

    public void Attach(GameObject _newChild)
    {
        _newChild.transform.SetParent(transform);
    }

    public void Dettach(GameObject _newChild)
    {
        _newChild.transform.SetParent(null);
    }

}
