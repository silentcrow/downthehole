﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnController : MonoBehaviour
{

    public GameObject currentRespawn;
    public List<GameObject> respawns;

    // Start is called before the first frame update
    void Start()
    {
        foreach(Transform child in transform)
        {
            respawns.Add(child.gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateRespawn(GameObject _newRespawn)
    {
        currentRespawn = _newRespawn;
        foreach(GameObject respawn in respawns)
        {
            respawn.GetComponent<Respawn>().Deactivate();
        }

        _newRespawn.GetComponent<Respawn>().Activate();

    }


}
