﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMov : MonoBehaviour
{
    private PlayerCollisions coll;
    private Rigidbody2D rb;
    private Animator anim;

    [Space]
    [Header("Stats")]
    public float speed = 10;
    public float jumpForce = 50;
    public float slideSpeed = 5;
    public float dashSpeed = 20;
    public float grapleDistance = 2;


    [Space]
    public bool hasDashed;
    public bool canDash = true;
    private bool canMove = true;
    public bool canGraple = true;
    public Vector2 vel;

    [Space]
    [Header("Stats")]
    public GameObject rController;

    private bool isGrabing;
    private Vector3 hitPoint;
    private GameObject hitObj;
    private Vector3 hitdifference;
    private bool isAttached;
    private bool failedGrab;
    private float oldXPos;
    private bool isJumping;


    // Start is called before the first frame update
    void Start()
    {
        coll = GetComponent<PlayerCollisions>();
        rb = GetComponent<Rigidbody2D>();
        rController = GameObject.FindGameObjectWithTag("RespawnController");
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

        vel = rb.velocity;
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");
        float xRaw = Input.GetAxisRaw("Horizontal");
        float yRaw = Input.GetAxisRaw("Vertical");
        Vector2 dir = new Vector2(x, y);


        if (Input.GetButtonDown("Jump"))
        {
            Jump(Vector2.up);
        }

        if (Input.GetButtonDown("Dash"))
        {
            if (xRaw != 0 || yRaw != 0)
                Dash(xRaw, yRaw);
        }

        if (Input.GetButtonDown("Graple"))
        {
            Graple();
        }

        if (Input.GetButtonUp("Graple"))
        {
            GrapleOFF();
        }


        UpdateLineRender();
        CheckGrab();

        LerpDrag();
        CheckLanding();

        if (coll.onGround)
        {
            canDash = true;
            canGraple = true;
            anim.SetBool("Landing", true);
            
        }
        else
        {
            anim.SetBool("Landing", false);
        }
    }


    void FixedUpdate()
    {
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");
        float xRaw = Input.GetAxisRaw("Horizontal");
        float yRaw = Input.GetAxisRaw("Vertical");
        Vector2 dir = new Vector2(x, y);
        Vector2 dirRaw = new Vector2(xRaw, yRaw);

        if (canMove)
        {
            Walk(dir);
        }
        StopMov(dirRaw);
    }



    void Walk(Vector2 _dir)
    {
        if (isGrabing)
        {
            rb.velocity = new Vector2(0, _dir.y * speed);
        }
        else
        {
            if (_dir.x > 0)
            {
                transform.eulerAngles = new Vector3(0, 0, 0);
                if (coll.onRightWall)//against a wall
                {
                    rb.velocity = new Vector2(0, rb.velocity.y);
                }
                else
                {
                    if (!hasDashed)
                    {
                        rb.velocity = new Vector2(_dir.x * speed, rb.velocity.y);
                        anim.SetBool("isMoving", true);
                    }


                }
            }
            if (_dir.x < 0)
            {
                transform.eulerAngles = new Vector3(0, 180, 0);
                if (coll.onLeftWall)
                {
                    rb.velocity = new Vector2(0, rb.velocity.y);
                }
                else
                {
                    if (!hasDashed)
                    {
                        rb.velocity = new Vector2(_dir.x * speed, rb.velocity.y);
                        anim.SetBool("isMoving", true);
                    }
                }
            }
        }



    }


    private void StopMov(Vector2 _dir)
    {
        if (_dir.x == 0)
        {
            Vector2 vel = rb.velocity;
            vel.x = 0;
            rb.velocity = vel;
            anim.SetBool("isMoving", false);
        }
    }

    void ForceStopMovement()
    {
        rb.gravityScale = 0;
        rb.velocity = new Vector2(0, 0);
        canMove = false;
    }

    private void Jump(Vector2 dir)
    {
        if (coll.onGround || isGrabing)
        {
            GrapleOFF();
            anim.SetBool("Jump",true);
            isJumping = true;
            rb.velocity = new Vector2(rb.velocity.x, 0);
            rb.velocity += dir * jumpForce;

        }

    }

    void CheckLanding()
    {
        if (rb.velocity.y<0)
        {
            if (Physics2D.Raycast(transform.position - new Vector3(0, 0.5f, 0), -Vector2.up, 1f, coll.groundLayer))
            {
                anim.SetBool("Jump", false);

            }

        }

    }


    private void Dash(float x, float y)
    {
        if(canDash)
        {
            GrapleOFF();
            hasDashed = true;
            canDash = false;
            rb.velocity = Vector2.zero;
            Vector2 dir = new Vector2(x, y);

            rb.velocity += dir.normalized * dashSpeed;
            StartCoroutine(DashWait());
            
        }
       
    }

    IEnumerator DashWait()
    {
        coll.disableGroundCheck = true;

        StartCoroutine(GroundDash());

        rb.gravityScale = 0;
        rb.drag = 14;
        GetComponent<BetterJumping>().enabled = false;
        hasDashed = true;
        

        yield return new WaitForSeconds(.2f);

        hasDashed = false;
        rb.gravityScale = 3;
        GetComponent<BetterJumping>().enabled = true;

    }

    IEnumerator GroundDash()
    {
        yield return new WaitForSeconds(.15f);
        canDash = false;
        coll.disableGroundCheck = false;
        if (coll.onGround)
        {
            canDash = true;
            hasDashed = false;
        }
    }

    void LerpDrag()
    {
        if (hasDashed)
        {
            if (rb.drag > 0)
            {
                rb.drag -= Time.deltaTime * 60;
            }
        }
        else
        {
            rb.drag = 0;
        }
    }

    void Death()
    {
        Debug.Log("death test");
        StartCoroutine(DeathAndRespawn());
    }
    
    IEnumerator DeathAndRespawn()
    {

        ForceStopMovement();
        anim.SetBool("Jump", false);
        //death animation
        //rb.gravityScale = 0;
        anim.SetTrigger("Death");
        yield return new WaitForSeconds(0.5f);
        //fade animation
       
        yield return new WaitForSeconds(0.1f);
       
        //move character
        Vector3 newPos = rController.GetComponent<RespawnController>().currentRespawn.transform.position;
        newPos.y += 1;
        transform.position = newPos;
        anim.SetTrigger("Restart");
        rb.gravityScale = 3;
        canMove = true;


    }


    void Graple()
    {
        if(canGraple)
        {
            if (!isGrabing && !failedGrab)
            {
                RaycastHit2D hit = Physics2D.Raycast(transform.position + new Vector3(0, 0.5f, 0), Vector2.up, grapleDistance);//raycast up

                if (hit.collider != null)//find an obj
                {
                    GetComponent<LineRenderer>().SetPosition(0, transform.position + new Vector3(0, 0.5f, 0));
                    GetComponent<LineRenderer>().SetPosition(1, hit.point);
                    hitPoint = hit.point;
                    oldXPos = transform.position.x;
                    anim.SetBool("Grapling", true);
                    if (hit.collider.gameObject.CompareTag("MovePlatform"))
                    {
                        hit.collider.gameObject.GetComponent<MovingObj>().Attach(this.gameObject);
                        hitdifference = hit.transform.position - hitPoint;
                        hitObj = hit.collider.gameObject;
                        isAttached = true;
                    }
                    Debug.Log("test");
                    isGrabing = true;
                    rb.gravityScale = 0;
                    canGraple = false;

                }
                else
                {
                    StartCoroutine(FailGrap());
                }

            }
        }
        
        
        
    }

    void GrapleOFF()
    {
        if(isGrabing)
        {
            transform.SetParent(null);
            isGrabing = false;
            rb.gravityScale = 3;
            failedGrab = false;
            isAttached = false;
            anim.SetBool("Grapling", false);
            ResetLine();
        }
    }

    void UpdateLineRender()
    {
        if (isGrabing)
        {
            GetComponent<LineRenderer>().SetPosition(0, transform.position);
            if(isAttached)
            {
                hitPoint = hitObj.transform.position - hitdifference;
                GetComponent<LineRenderer>().SetPosition(1, hitPoint);
            }
        }
    }

    void ResetLine()
    {
        GetComponent<LineRenderer>().SetPosition(0, new Vector3(0, 0, 0));
        GetComponent<LineRenderer>().SetPosition(1, new Vector3(0, 0, 0));
        failedGrab = false;

    }

    IEnumerator FailGrap()
    {
        failedGrab = true;
        canGraple = false;
        GetComponent<LineRenderer>().SetPosition(0, transform.position);
        GetComponent<LineRenderer>().SetPosition(1, transform.position + new Vector3(0, grapleDistance, 0));
        yield return new WaitForSeconds(0.2f);
        canGraple = true;
        ResetLine();

    }


    void CheckGrab()
    {
    
        if(isGrabing)
        {
            //if player is pushed
            if(Mathf.Abs(hitPoint.x-transform.position.x)>0.2f)
            {
                GrapleOFF();
            }

            //if too far down the rope
            if(Vector3.Distance(transform.position,hitPoint)>grapleDistance)
            {
                GrapleOFF();
            }

            //above hit point
            if(transform.position.y>hitPoint.y)
            {
                GrapleOFF();
            }
        }
    }

  
    ///COLLISIONS/////

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.CompareTag("Respawn"))
        {
            col.gameObject.GetComponent<Respawn>().CollideActivate();
        }

    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("Death"))
        {
            Death();
        }
        else if (col.gameObject.CompareTag("MovePlatform"))
        {
            if(!isGrabing)
            {
                col.gameObject.GetComponent<MovingObj>().Attach(this.gameObject);
            }
           
        }
    }

    void OnCollisionExit2D(Collision2D col)
    {
       if (col.gameObject.CompareTag("MovePlatform"))
        {
            col.gameObject.GetComponent<MovingObj>().Dettach(this.gameObject);
        }
    }


}
